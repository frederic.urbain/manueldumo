# Glossaire

<dl>

<dt>Application</dt>
<dd>Programme (logiciel) destiné à l'utilisateur et servant à réaliser des tâches dans un domaine particulier (traitement de texte, navigation web, etc.). Pour ne pas confondre application et logiciel, voyez «&nbsp;Logiciel&nbsp;», plus loin dans le Glossaire.</dd>

<dt>Chiffrement</dt>
<dd>Un procédé (de cryptographie) qui rend impossible la compréhension d'un message sans une clé de déchiffrement.</dd>


<dt>Client (logiciel)</dt>
<dd>Un logiciel client est un logiciel qui permet de communiquer avec un serveur et de récupérer ou envoyer des données selon des protocoles définis. Un client de messagerie électronique est un logiciel qui récupère et envoie des messages électroniques sur un serveur selon les protocoles IMAP, POP, Exchange, etc.</dd>

<dt>Cookie</dt>
<dd>Un témoin de connexion qui fait office de jeton afin d'authentifier ou attester la connexion entre deux machines. Un cookie peut aussi véhiculer d'autres informations.</dd>

<dt>Cracker</dt>
<dd>Un amateur ou un expert spécialisé dans le cassage des protections de sécurité des logiciels.</dd>

<dt>Cryptographie</dt>
<dd>Art (ou discipline) de protéger des messages en les rendant inintelligibles, notamment au moyen du chiffrement.</dd>

<dt>Dépôt (logiciels)</dt>
<dd>Un dépôt est un service de stockage et diffusion de logiciels. Pour les utilisateurs de GNU/Linux, chaque distribution propose des dépôts validés. On s'y connecte via un gestionnaire de dépôts afin de télécharger et installer les programmes que l'on souhaite. Si un logiciel ne se trouve pas dans les dépôts, c'est qu'il n'a pas encore été validé ou qu'il ne présente pas les caractéristiques suffisantes de stabilité et de sécurité.  Selon votre système d'exploitation, les dépôts sont parfois appelés <i>store</i> ou <i>magasin</i>. </dd>

<dt>Distribution</dt>
<dd>Une distribution est un ensemble cohérent de logiciels permettant d'effectuer un ensemble de tâches spécialisées. Les différentes versions de systèmes d'exploitation GNU/Linux sont des distributions : elles répondent à des besoins différents, ne proposent pas toutes les mêmes logiciels.</dd>

<dt>Extension (fichier)</dt>
<dd>Quelques caractères à la fin d'un nom de fichier, après un point, servant à identifier rapidement le format du fichier.</dd>

<dt>Extension (logiciel)</dt>
<dd>Une extension ou un <i>module</i> d'extension ou encore un <i>plugin</i> est une série de commandes se greffant à un logiciel <i>hôte</i> afin d'étendre ses fonctionnalités.</dd>

<dt>Format (fichier)</dt>
<dd>Un format de fichier est une convention qui représente la manière dont sont arrangées et stockées les données regroupées dans ce fichier.</dd>

<dt>Hacker</dt>
<dd>Un passionné d'informatique, qui apprécie notamment de bidouiller en vue d'améliorer des programmes ou du matériel. </dd>

<dt>Internet</dt>
<dd>Un réseau informatique mondial accessible au public. Les échanges s'y font grâce à des protocoles standards de communication.</dd>

<dt>Interopérabilité (des formats)</dt>
<dd>L’interopérabilité d'un format de fichier est sa capacité à fonctionner ou être traité par des logiciels différents.</dd>


<dt>Libre (logiciel)</dt>
<dd>Un logiciel libre est un logiciel placé sous une licence qui respecte les quatre libertés logicielles : 1.&nbsp;la liberté d'utiliser le logiciel, 2.&nbsp;la liberté de copier le logiciel, 3.&nbsp;la liberté d'étudier le logiciel, 4.&nbsp;la liberté de modifier le logiciel et de redistribuer les versions modifiées.</dd>


<dt>Licence libre</dt>
<dd>Un contrat qui formalise l'exercice des quatre libertés logicielles (lorsqu'il s'agit d'un logiciel libre) ou qui s'inspire de ces quatre libertés pour contractualiser l'usage d'un contenu ou d'une œuvre.</dd>


<dt>Logiciel</dt>
<dd>Ensemble d'instructions (programme(s)) et jeu de données (fichiers) qu'un utilisateur ou une machine utilise pour effectuer des tâches. Un logiciel de traitement de texte est un programme qu'on peut qualifier d'applicatif (voir «&nbsp;Application&nbsp;»). Un noyau de système d'exploitation est un programme qui n'a pas une visée applicative.</dd>

<dt>Navigateur (Web)</dt>
<dd>Un navigateur est un logiciel permettant d'afficher le Web, en particulier en utilisant le protocole HTTP.</dd>

<dt>Nétiquette</dt>
<dd>Ensemble de règles informelles visant à structurer les bonnes pratiques d'usage de la messagerie entre internautes.</dd>

<dt>Noyau (système d'exploitation)</dt>
<dd>Un noyau est un programme à la base du système d'exploitation. Il permet de gérer les accès aux dispositifs de calcul et de mémoire, coordonne les programmes et les éléments matériels. Linux est le noyau d'un système GNU/Linux.</dd>

<dt>Piratage</dt>
<dd>Action illégale dans le domaine informatique : contrefaire des œuvres, s'approprier des données, usurper des identités etc.</dd>

<dt>Port (matériel)</dt>
<dd>Une prise permettant de brancher un périphérique sur un ordinateur.</dd>

<dt>Port (logiciel)</dt>
<dd>Un système permettant de gérer les entrées et sorties d'information.</dd>

<dt>Propriétaire (ou privateur)</dt>
<dd>Un programme est dit propriétaire s'il ne permet pas l'exercice des 4 libertés propres au logiciel libre. La principale caractéristique est la non diffusion du code source et l'impossibilité d'un audit public du logiciel. Il constitue donc une porte ouverte pour un usage déloyal des données des utilisateurs qu'il traite. C'est pour cette raison que le terme « privateur » (de libertés) est aussi employé. </dd>

<dt>Protocole de communication</dt>
<dd>Une suite d'opérations nécessaires pour mettre en relation deux machines sur un réseau. Un protocole peut intégrer le procédé de mise en relation, la convention d'échange, le contrôle de la communication.</dd>

<dt>RFC</dt>
<dd><i>Request for comment</i> : ce sont des documents officiels rédigés sur la base de commentaires calibrés et servant à décrire les aspects techniques d'internet après délibération. Certaines RFC sont devenues des standards.</dd>

<dt>Système d'exploitation</dt>
<dd>Un ensemble de programmes qui permet d'exploiter les ressources d'un ordinateur (matériel, surtout).</dd>

<dt>Virus (informatique)</dt>
<dd>Un programme qui se propage d'un ordinateur à l'autre. Il peut contenir du code malveillant qui agit par exemple en supprimant des données ou en les chiffrant (pour permettre à ses commanditaires de réclamer une rançon en échange de la clé de déchiffrement).</dd>

<dt>Web (World Wide Web)</dt>
<dd>C'est le système hypertexte fonctionnant sur le réseau Internet, grâce à un navigateur qui permet de récupérer des contenus. L'image de la toile (<i>web</i>) est employée pour illustrer les liens hypertextes qui lient les pages entres elles.</dd>

<dt>Webmail</dt>
<dd>Un client de messagerie électronique disposant d'une interface Web permettant de gérer son courrier en ligne à l'aide d'un navigateur.</dd>



</dl>







